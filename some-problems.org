#+TITLE: Some Problems


*  Calculating with Functions

    #+begin_src js
    'zero one two three four five six seven eight nine'.split(' ').forEach(
    (mth, num) => this[mth] = (f = val => val) => f(num)
    )

    let plus      = (r) => (l) => l + r
    let minus     = (r) => (l) => l - r
    let times     = (r) => (l) => l * r
    let dividedBy = (r) => (l) => l / r

    #+end_src

    #+begin_src js
    const
    id = x => x,
    number = x => (f = id) => f(x),
    [zero, one, two, three, four, five, six, seven, eight, nine] =
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map(number),
    plus = x => y => y + x,
    minus = x => y => y - x,
    times = x => y => y * x,
    dividedBy = x => y => y / x;
    #+end_src

    #+begin_src js
    function zero(cb) {
    if(cb !== undefined){
        return cb(0);
    }
    return 0;
    }
    function one(cb) {
    if(cb !== undefined){
        return cb(1);
    }
    return 1;
    }
    function two(cb) {
    if(cb !== undefined){
        return cb(2);
    }
    return 2;
    }
    function three(cb) {
    if(  cb !== undefined){
        return cb(3);
    }
    return 3;
    }
    function four(cb) {
    if(  cb !== undefined){
        return cb(4);
    }
    return 4;
    }
    function five(cb) {
    if(  cb !== undefined){
        return cb(5);
    }
    return 5;
    }
    function six(cb) {
    if(  cb !== undefined){
        return cb(6);
    }
    return 6;
    }
    function seven(cb) {
    if(  cb !== undefined){
        return cb(7);
    }
    return 7;
    }
    function eight(cb) {
    if(  cb !== undefined){
        return cb(8);
    }
    return 8;
    }
    function nine(cb) {
    if(  cb !== undefined){
        return cb(9);
    }
    return 9;
    }

    function plus(num) {
    return (num2)=>{
        return num2 + num;
    }
    }
    function minus(num) {
    return (num2)=>{
        return num2 - num;
    }
    }
    function times(num) {
    return (num2)=>{
        return num2 * num;
    }
    }
    function dividedBy(num) {
    return (num2)=>{
        return Math.ceil(num2 /num);
    }
    }
    #+end_src

*  Count char in given string
- simple version not check is alpha numeric
#+begin_src js

function countChar(s){
    let str = s.toLowerCase();
    let freq = {};
    for(let c of str){
        // ++freq[c] work but freq[c]++ not work
        freq[c] = ++freq[c] || 1;
    }
    return freq;
}
console.log(countChar('hello'))
console.log(countChar('hello World!'))

#+end_src

#+RESULTS:
: { h: 1, e: 1, l: 2, o: 1 }
: { h: 1, e: 1, l: 3, o: 2, ' ': 1, w: 1, r: 1, d: 1, '!': 1 }

we can use regex to check it but we have a better solution

#+begin_src js
function isAlphaNumeric(c){
    let code = c.charCodeAt(0);
    if(
        !(code >47 && code<58) && // 0-9
            !(code >64 && code<91) && // A-Z
            !(code >96 && code<123)    // a-z
    ){
        return false;
    }
    return true;
}
function countChar(s){
    let str = s.toLowerCase();
    let freq = {};
    for(let c of str){
        if(isAlphaNumeric(c)){
            // ++freq[c] work but freq[c]++ not work
            freq[c] = ++freq[c] || 1;
        }
    }
    return freq;
}
console.log(countChar('hello'))
console.log(countChar('hello World!'))
#+end_src

#+RESULTS:
: { h: 1, e: 1, l: 2, o: 1 }
: { h: 1, e: 1, l: 3, o: 2, w: 1, r: 1, d: 1 }
